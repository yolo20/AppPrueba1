import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OlvideDatosPage } from './olvide-datos.page';

describe('OlvideDatosPage', () => {
  let component: OlvideDatosPage;
  let fixture: ComponentFixture<OlvideDatosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OlvideDatosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OlvideDatosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
