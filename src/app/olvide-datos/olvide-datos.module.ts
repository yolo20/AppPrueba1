import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OlvideDatosPageRoutingModule } from './olvide-datos-routing.module';

import { OlvideDatosPage } from './olvide-datos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OlvideDatosPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [OlvideDatosPage]
})
export class OlvideDatosPageModule {}
