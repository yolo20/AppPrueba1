import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OlvideDatosPage } from './olvide-datos.page';

const routes: Routes = [
  {
    path: '',
    component: OlvideDatosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OlvideDatosPageRoutingModule {}
