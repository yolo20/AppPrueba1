import { Component, OnInit } from '@angular/core';
//Importamos el componente toast
import { ToastController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-olvide-datos',
  templateUrl: './olvide-datos.page.html',
  styleUrls: ['./olvide-datos.page.scss'],
})
export class OlvideDatosPage implements OnInit {

  forgotPassword: FormGroup;

  constructor(
    private toastController: ToastController, private authServise: AuthService,
    private  formBuilder: FormBuilder, private router: Router
    ) {}

  ngOnInit() {
    this.forgotPassword = this.FormforgotPassword(); 
  }

//Envia link de restauración de contraseña
  sendLinkReset(value){
    this.authServise.resetPassword(value).then(()=>{
      this.presentToast();
      this.router.navigate(['/iniciarsesion']);
    }).catch(err => alert(err));
  }
//Condiciones de la entrada de datos form
  private FormforgotPassword() {
    return this.formBuilder.group({
      email:['', [Validators.required, Validators.email]]
    })
  }
// Mesajes de error en las entradas de datos de los forms
  validation_messages = {
    'email': [
      { type: 'required', message: 'Correo is required.' },
      { type: 'email', message: 'Please enter a valid email.' }
    ]
  }
// Función que modifica el texto que aparecera al hacer click en el boton ok 
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Se envió un correo electrónico con un enlace para restablecer su contraseña.',
      duration: 6000,
      position: 'middle', 
    });
    toast.present();
  }
  toast(){
    this.presentToast();
  }

}
