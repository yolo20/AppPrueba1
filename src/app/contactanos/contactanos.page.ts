import { Component, OnInit } from '@angular/core';

//Agregamos el componentes
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contactanos',
  templateUrl: './contactanos.page.html',
  styleUrls: ['./contactanos.page.scss'],
})
export class ContactanosPage implements OnInit {

  FormContactanos: FormGroup;

  constructor(
      private emailComposer: EmailComposer, private formBuilder: FormBuilder,
    ) {}

  ngOnInit() {
    this.FormContactanos = this.FormValid();
  }

//Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode){
    if (keyCode === 13) {
      if(this.FormContactanos.valid){
        // Trigger the button element with a click
        document.getElementById("bttSubmitContactanos").click();
      }
    } 
  }
// Condiciones de la entrada de datos form
  private FormValid(){
    return this.formBuilder.group({
      nombre:['',[Validators.required, Validators.pattern('[A-Za-z ]*'), Validators.minLength(2)]],
      apellido:['',[Validators.required, Validators.pattern('[A-Za-z ]*'), Validators.minLength(2)]],
      email:['',[Validators.required, Validators.email]],
      telefono:['',[Validators.required, Validators.pattern('[0-9 ]*')]],
      bodymessage:['',[Validators.required]]
    })
  }  
// Mesajes de error en las entradas de datos de los forms
  validation_messages = {
    'telefono':[
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Please enter a valid phone.' },
      // { type: 'minlength', message: 'Phone must be at least 10 characters long.' }   
    ],
    'nombre': [
      { type: 'required', message: 'Name is required.' },
      { type: 'pattern', message: 'Please enter a valid name.' },
      { type: 'minlength', message: 'Name must be at least 2 characters long.' }
    ],
    'apellido': [
      { type: 'required', message: 'Last name is required.' },
      { type: 'pattern', message: 'Please enter a valid last name.' },
      { type: 'minlength', message: 'Last name must be at least 2 characters long.' }
    ], 
    'email': [
      { type: 'required', message: 'Correo is required.' },
      { type: 'email', message: 'Please enter a valid email.' }
    ],
    'bodymessage':[
      { type: 'required', message: 'Message body is required.' },
    ]
  }
 // Envia un mensaje por el correo electronico del usuario
  sendEmail(value){
    let email = {
      to: '10rairelimpio@gmail.com',
      subject: 'PQR',
      body: 'Yo: '+value.nombre+' '+value.apellido+'\n'+'con correo electronico: '+ value.email+'\n'+'y celular: '+value.telefono+'\n'+'El motivo o razón de consulta es: '+value.bodymessage,
      isHtml: true
    }
    // Send a text message using default options
    this.emailComposer.open(email);
    this.FormContactanos.reset(); // borramos los datos del form
  }

}
