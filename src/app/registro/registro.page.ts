import { Component, OnInit } from '@angular/core';

import { ToastController, NavController } from '@ionic/angular'; //Importamos el componente toast para mostrar un texto emergente
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms'; // componentes forms
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { FirestoreService } from '../services/data/firestore.service'
import { AppComponent } from '../app.component';
import { areAllEquivalent } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  RegisPNatural: FormGroup;
  RegisEmpresa: FormGroup;
  activate = false;
  hide: boolean;
  hide1:boolean;

  constructor(
    private firestoreDB : FirestoreService, private toastController: ToastController, 
    private authService: AuthService, private router: Router,
    private formBuilder: FormBuilder, private app: AppComponent
    ) {}

  ngOnInit() {
    this.RegisPNatural = this.FormPNatural();
    this.RegisEmpresa = this.FormEmpresa();
  }
  
//Registrar con Firebase (Contraseña y correo)
  RegistroEmailPassword(value)
  {
    const user = this.authService.onRegister(value).then( () =>{
      this.router.navigate(['/iniciarsesion']);
      }).catch(err => { alert(err); })
    
  }
// Donde se guardan los datos del form
  saveDataPNatural(data){
      this.RegistroEmailPassword(data);
      this.RegisEmpresa.reset(); // borramos los datos del form
  }
  saveDataEmpresa(data){
    this.RegistroEmailPassword(data);
    this.RegisPNatural.reset(); // borramos los datos del form
  }
//Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode){
    if (keyCode === 13) {
      if(this.RegisPNatural.valid){
        // Trigger the button element with a click
        document.getElementById("bttSubmitPNatural").click();
      }
    } 
  }
  EventHandler(keyCode){
    if (keyCode === 13) {
      if(this.RegisEmpresa.valid){
        // Trigger the button element with a click
        document.getElementById("bttSubmitEmpresa").click();
      }
    } 
  }
// Verifica la contraseña en el form
  matchOtherValidator (otherControlName: string) {
    let thisControl: FormControl;
    let otherControl: FormControl;
    return function matchOtherValidate (control: FormControl) {
      if (!control.parent) {
        return null;
      }
      // Initializing the validator.
      if (!thisControl) {
        thisControl = control;
        otherControl = control.parent.get(otherControlName) as FormControl;
        if (!otherControl) {
          throw new Error('matchOtherValidator(): other control is not found in parent group');
        }
        otherControl.valueChanges.subscribe(() => {
          thisControl.updateValueAndValidity();
        });
      }
      if (!otherControl) {
        return null;
      }
      if (otherControl.value !== thisControl.value) {
        return {
          matchOther: true
        };
      }
      return null;
    }
  }
// Condiciones de la entrada de datos form
  private FormEmpresa(){
    return this.formBuilder.group({
      nit:['', [Validators.required, Validators.minLength(2)]],
      nombreEmpresa: ['', [Validators.required, Validators.minLength(2)]],
      actividadEmpresa:['', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z ]*')]],
      nombre: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      apellido: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      celular: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[0-9]*')]],
      tipodocumento:['', Validators.required],
      documento: ['', [Validators.required, Validators.minLength(8), Validators.pattern('[A-Za-z0-9]*')]],
      direccion:   ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      passwordRetry: this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*\\d)(?=.*[0-9])(?=.*[a-záéíóúüñ]).*[A-ZÁÉÍÓÚÜÑ].*')]],
        passwordConfirmation: ['', [Validators.required, Validators.minLength(6), this.matchOtherValidator('password')]]
      })
    });
  }
// Condiciones de la entrada de datos form
  private FormPNatural(){
    return this.formBuilder.group({
      nombre: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      apellido: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      passwordRetry: this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*\\d)(?=.*[0-9])(?=.*[a-záéíóúüñ]).*[A-ZÁÉÍÓÚÜÑ].*')]],
        passwordConfirmation: ['', [Validators.required, Validators.minLength(6), this.matchOtherValidator('password')]]
      }),
    });
  }
// Mesajes de error en las entradas de datos de los forms
  validation_messages = {
    'nit':[
      { type: 'required', message: 'Nit is required.' },
      // { type: 'pattern', message: 'Please enter a valid name.' },
      { type: 'minlength', message: 'Nit must be at least 2 characters long.' }
    ],
    'nombreEmpresa': [
      { type: 'required', message: 'Company name is required.' },
      // { type: 'pattern', message: 'Please enter a valid name.' },
      { type: 'minlength', message: 'Company name must be at least 2 characters long.' }
    ],
    'actividadEmpresa':[
      { type: 'required', message: 'Company activity is required.' },
      { type: 'pattern', message: 'Please enter a valid Company activity.' },
      { type: 'minlength', message: 'Company activity must be at least 2 characters long.' }
    ],
    'celular':[
      { type: 'required', message: 'Cell phone is required.' },
      { type: 'pattern', message: 'Please enter a valid cell phone.' },
      { type: 'minlength', message: 'Cell phone must be at least 10 characters long.' }
    ],
    'tipodocumento':[
      { type: 'required', message: 'Document type is required.' },
      // { type: 'pattern', message: 'Please enter a valid name.' },
      // { type: 'minlength', message: 'Name must be at least 2 characters long.' }
    ],
    'documento': [
      { type: 'required', message: 'Document is required.' },
      { type: 'pattern', message: 'Please enter a valid document.' },
      { type: 'minlength', message: 'Document must be at least 8 characters long.' }
    ],
    'direccion': [
      { type: 'required', message: 'Direction is required.' },
      // { type: 'pattern', message: 'Please enter a valid name.' },
      { type: 'minlength', message: 'Direction must be at least 2 characters long.' }
    ],
    'nombre': [
      { type: 'required', message: 'Name is required.' },
      { type: 'pattern', message: 'Please enter a valid name.' },
      { type: 'minlength', message: 'Name must be at least 2 characters long.' }
    ],
    'apellido': [
      { type: 'required', message: 'Last name is required.' },
      { type: 'pattern', message: 'Please enter a valid last name.' },
      { type: 'minlength', message: 'Last name must be at least 2 characters long.' }
    ],
    'email': [
      { type: 'required', message: 'Correo is required.' },
      { type: 'email', message: 'Please enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' },
      { type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number.' }
    ],
    'passwordConfirmation': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' },
      { type: 'matchOther', message: 'Password mismatch.' }
    ]
  }
// Función que mostrara un texto al hacer click o enter  en correo
  async presentToast1() {
    const toast = await this.toastController.create({
      message: 'Su correo electronico sera usado como usuario en el inicio de sesión.',
      duration: 4000,
      position: 'top'
    });
    toast.present();
  }
  Event(keyCode){
    if (keyCode === 13) {
      this.presentToast1();
      
    } 
  }
//Función del boton Empresas esconde lo de Natural y muestra lo de Empresas
  bttregistroEmpresa() {
    document.getElementById('personaNatural').style.display="none";
    document.getElementById('Empresa').style.display="block";
    if(this.activate == false){
      this.activate = true;
    }
   
  }

// Función del boton Natural esconde lo de Empresas y muestra lo de Natural
  bttregistroNatural() {
    document.getElementById('Empresa').style.display="none";
    document.getElementById('personaNatural').style.display="block";
    if(this.activate == true){
      this.activate = false;
    }
  }

}

 