import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-patrocinadores',
  templateUrl: './patrocinadores.page.html',
  styleUrls: ['./patrocinadores.page.scss'],
})
export class PatrocinadoresPage implements OnInit {

  
// show controla cuando se muestran los slides
  activate = false;
// Aplicación de estilos para el cambio del slide
  slideOpts = {
    autoplay:true
  };

  constructor() { }

  ngOnInit() { }
  

//Función del boton Empresas esconde lo de Natural y muestra lo de Empresas
  patrocinoEmpresas() {
    document.getElementById('Empresas').style.display="block";
    document.getElementById('PersonaNatural').style.display="none"; 
    if(this.activate == false){
      this.activate = true;
    }
  }
// Función del boton Natural esconde lo de Empresas y muestra lo de Natural
  patrocinoNatural() {
    document.getElementById('PersonaNatural').style.display="block";
    document.getElementById('Empresas').style.display="none";
    if(this.activate == true){
      this.activate = false;
    }
  }


}
