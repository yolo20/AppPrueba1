import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { NavController } from '@ionic/angular';
import { RouterLink, Router } from '@angular/router';
import { AppComponent } from '../app.component';


@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
// Activa los slides
  slideOpts = {
    autoplay:true
  }

  constructor(
    private app: AppComponent, public router : Router , public auth: AuthService
    ) { }

  ngOnInit() {}

  iraHuella(){
    if(this.auth.User == true){
    this.router.navigate(['/huelladecarbono']);
    this.app.selectedIndex = 4;
    }else{
    this.router.navigate(['/huelladecarbono']);
    this.app.selectedIndex = 2;
    ;}
  }
  iraPatrocinadores(){
    if(this.auth.User == false){
    this.router.navigate(['/iniciarsesion']);
    this.app.selectedIndex = 5;
    }else{
    this.router.navigate(['/patrocinadores']);
    this.app.selectedIndex = 3;
    ;}
  }
  iraRegistrarProyecto(){
    if(this.auth.User == false){
    this.router.navigate(['/iniciarsesion']);
    this.app.selectedIndex = 5;
    }else{
    this.router.navigate(['/basededatos']);
    this.app.selectedIndex = 2;
    ;}
  }
}
