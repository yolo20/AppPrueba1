import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPagesNotAuthorized = [
    {
      title: 'Inicio',
      url: '/folder/Index',
      icon: 'Home'
    },
    {
      title: 'Acerca de nosotros',
      url: 'acercadenosotros',
      icon: 'heart'
    },
    {
      title: 'Huella de carbono',
      url: 'huelladecarbono',
      icon: 'flower'
    },
    {
      title: 'Beneficios',
      url: 'descuentos',
      icon: 'pricetag'
    },
    {
      title: 'Contáctenos',
      url: 'contactanos',
      icon: 'card'
    },    
    {
      title: 'Iniciar sesión',
      url: 'iniciarsesion',
      icon: 'log-in'
    }
  ];
  public appPages = [
    {
      title: 'Inicio',
      url: '/folder/Index',
      icon: 'Home'
    },
    {
      title: 'Acerca de nosotros',
      url: 'acercadenosotros',
      icon: 'heart'
    },
    {
      title: 'Banco de proyectos',
      url: 'basededatos',
      icon: 'document'
    },
    {
      title: 'Patrocinadores',
      url: 'patrocinadores',
      icon: 'ribbon'
    },
    {
      title: 'Huella de carbono',
      url: 'huelladecarbono',
      icon: 'flower'
    },
    {
      title: 'Beneficios',
      url: 'descuentos',
      icon: 'pricetag'
    },
    {
      title: 'Mi cuenta',
      url: 'micuenta',
      icon: 'person'
    },
    {
      title: 'Contáctenos',
      url: 'contactanos',
      icon: 'card'
    },    
    {
      title: 'Cerrar sesión',
      url: '/folder/Index',
      icon: 'log-out',
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public auth: AuthService
  ) {
    this.initializeApp();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
      if (path !== undefined) {
        this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
      }
  }

  onLogout(selectedIndex)
  {
    if (selectedIndex == 8){
      this.auth.logout();
      this.selectedIndex = 0;
    }
  }

}