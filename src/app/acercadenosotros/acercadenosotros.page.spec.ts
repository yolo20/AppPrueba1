import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AcercadenosotrosPage } from './acercadenosotros.page';

describe('AcercadenosotrosPage', () => {
  let component: AcercadenosotrosPage;
  let fixture: ComponentFixture<AcercadenosotrosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcercadenosotrosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AcercadenosotrosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
