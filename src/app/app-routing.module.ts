import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'folder/Inicio', // Modificando esto se puede cambiar la pagina que aparece despues de la carga inicial
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'patrocinarproyecto',
    loadChildren: () => import('./patrocinarproyecto/patrocinarproyecto.module').then( m => m.PatrocinarproyectoPageModule)
  },
  {
    path: 'acercadenosotros',
    loadChildren: () => import('./acercadenosotros/acercadenosotros.module').then( m => m.AcercadenosotrosPageModule)
  },
  {
    path: 'contactanos',
    loadChildren: () => import('./contactanos/contactanos.module').then( m => m.ContactanosPageModule)
  },
  {
    path: 'registrodeproyecto',
    loadChildren: () => import('./registrodeproyecto/registrodeproyecto.module').then( m => m.RegistrodeproyectoPageModule)
  },
  {
    path: 'iniciarsesion',
    loadChildren: () => import('./iniciarsesion/iniciarsesion.module').then( m => m.IniciarsesionPageModule)
  },
  {
    path: 'registro',
    loadChildren: () => import('./registro/registro.module').then( m => m.RegistroPageModule)
  },
  {
    path: 'descuentos',
    loadChildren: () => import('./descuentos/descuentos.module').then( m => m.DescuentosPageModule)
  },
  {
    path: 'olvide-datos',
    loadChildren: () => import('./olvide-datos/olvide-datos.module').then( m => m.OlvideDatosPageModule)
  },
  {
    path: 'actualizar-datos',
    loadChildren: () => import('./actualizar-datos/actualizar-datos.module').then( m => m.ActualizarDatosPageModule)
  },
  {
    path: 'micuenta',
    loadChildren: () => import('./micuenta/micuenta.module').then( m => m.MicuentaPageModule)
  },
  {
    path: 'basededatos',
    loadChildren: () => import('./basededatos/basededatos.module').then( m => m.BasededatosPageModule)
  },
  {
    path: 'patrocinadores',
    loadChildren: () => import('./patrocinadores/patrocinadores.module').then( m => m.PatrocinadoresPageModule)
  },
  {
    path: 'huelladecarbono',
    loadChildren: () => import('./huelladecarbono/huelladecarbono.module').then( m => m.HuelladecarbonoPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}


