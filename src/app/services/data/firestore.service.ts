import { Injectable } from '@angular/core';

import { AngularFirestore } from "@angular/fire/firestore";
import { AuthService } from "../auth.service"

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  getNombreProyecto

  constructor(
    private auth: AuthService,
    private db: AngularFirestore
  ) { }

  // CRUD Leer 
  read(collectionName, documentID) {
    try {
      return this.db.collection(collectionName).doc(documentID).valueChanges();
    } catch (error) {
      console.log('Error on read data',error);
    }
    // return this.db.collection(collectionName).snapshotChanges();
  }

  //CRUD Actualizar Usuario
  updateUser(record) {
    try {
      const uid = this.auth.UIDuser;
    if (record.nit == null) {
      this.db.collection('users').doc(uid).update({
          nombre: record.nombre,
          apellido: record.apellido,
          celular: record.celular,
          telefono: record.telefono,
          direccion: record.direccion,
          pais:record.pais,
          departamento:record.departamento,
          municipio:record.municipio,
          tipodocumento:record.tipodocumento,
          documento: record.documento,
      });
    } else {
      this.db.collection('users').doc(uid).update({
        nit: record.nit,
        nombreEmpresa: record.nombreEmpresa,
        actividadEmpresa: record.actividadEmpresa,
        nombre: record.nombre,
        apellido: record.apellido,
        celular: record.celular,
        telefono: record.telefono,
        direccion: record.direccion,
        pais:record.pais,
        departamento:record.departamento,
        municipio:record.municipio,
        tipodocumento:record.tipodocumento,
        documento: record.documento,
      });
    }
    } catch (error) {
      console.log('Error on update user',error);
    }

  }
  //CRUD Agregar
  create(record,collection) {
    try {
        const uid = this.auth.UIDuser;
        if (collection == "RegistrarProyecto") {
            return this.db.collection(collection).doc(record.nombreProyecto).update({
              pais:record.pais,
              departamento:record.departamento,
              municipio:record.municipio,
              zona:record.zona,
              msnm:record.msnm,
              latitud:record.latitud,
              longitud:record.longitud,
              nombrepredio:record.nombrepredio,
              cedulacatastral:record.cedulacatastral,
              matricula:record.matricula,
              tipoproyecto: record.tipoproyecto,
              areaBosque:record.areaBosque,
              tipoBosque:record.tipoBosque,
              temperatura:record.temperatura,
              tipoTemperatura:record.tipoTemperatura,
              usoActual:record.usoActual,
              uid : uid,
            })
        } else {
          return this.db.collection(collection).doc(record.nombreProyecto).set({
            datatimeInicio: record.datatimeInicio,
            datatimeFin: record.datatimeFin,
            rublo: record.rublo,
            cantidadDinero:record.cantidadDinero,
            Frecuenciadelaporte:record.Frecuenciadelaporte,
            uid : uid,
          });
        }  
    } catch (error) {
      console.log('Error on add user',error);
    }
  }

  // create(collection,record) {
  //   return this.db.collection(this.collectionName).add(record);
  // }

  // delete(record_id) {
  //   this.db.doc(this.collectionName + '/' + record_id).delete();
  // }

}



// import { Injectable } from '@angular/core';
// import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
// import { Observable } from 'rxjs';
// import { map } from 'rxjs/operators';
// import { TaskI } from '../models/task.interface';

// @Injectable({
//   providedIn: 'root'
// })
// export class TodoService {
  
//   private todosCollection: AngularFirestoreCollection<TaskI>;
//   private todos: Observable<TaskI[]>;

//   constructor(db:AngularFirestore) { 
//     this.todosCollection = db.collection<TaskI>('todos');
//     this.todos = this.todosCollection.snapshotChanges().pipe(
//       map(actions => {
//         return actions.map(a => {
//           const data = a.payload.doc.data();
//           const id = a.payload.doc.id;
//           return {id, ...data};
//         });
//       })
//     );
//   }

//   getTodos(){
//     return this.todos;
//   }

//   getTodo(id: string){
//     return this.todosCollection.doc<TaskI>(id).valueChanges();
//   }

//   updateTodo(todo:TaskI, id: string){
//     return this.todosCollection.doc(id).update(todo);
//   }
  
//   addTodo(todo: TaskI){
//     return this.todosCollection.add(todo);
//   }
  
//   removeTodo(id: string){
//     return this.todosCollection.doc(id).delete();
//   }

// }
