import { Injectable } from '@angular/core';
//importamos componentes firebase
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";
import { AngularFirestore } from "@angular/fire/firestore";
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { auth } from 'firebase';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  UIDuser
  User = false;

  constructor( 
    private facebook: Facebook,private toastController: ToastController,
    private googleplus: GooglePlus,  private AFauth : AngularFireAuth,
    private router : Router, private db : AngularFirestore) 
  { }

//Reset de contraseña por medio de email
  resetPassword(value){
    try {
      return this.AFauth.auth.sendPasswordResetEmail(value.email);
    } catch (error) {
      console.log(error);
    }
  }

//login with Facebook
  loginWithFacebook(){
    try {
      return this.facebook.login(['public_profile', 'email']).then((response: FacebookLoginResponse)=>{
        const credential_data_Fb = auth.FacebookAuthProvider.credential(response.authResponse.accessToken);
        this.User = true; 
        this.UIDuser = response.authResponse.userID;
          this.db.collection('users').doc(response.authResponse.userID).set({
            apellido: '',
            nombre: '',
          });
        return this.AFauth.auth.signInWithCredential(credential_data_Fb);
      })
    } catch (error) {
      console.log('Error on Logout user',error);
      alert(error);
    }
  }

// login with google 
  loginWithGoogle () {
    try {
      return this.googleplus.login({}).then(result =>{
        const user_Data_Google = result; //data the google
        this.UIDuser = user_Data_Google.userId;
        this.User = true; 
        this.db.collection('users').doc(user_Data_Google.userId).set({
          nombre: user_Data_Google.displayName
        });
        return this.AFauth.auth.signInWithCredential(auth.GoogleAuthProvider.credential(null, user_Data_Google.accessToken));
      });
    } catch (error) {
      console.log('Error on Login user',error);
      alert(error);
    }
    
  }

// Login with email and password
  onLogin(value) 
  {
    return new Promise<any>((resolve, reject) => { 
      this.AFauth.auth.signInWithEmailAndPassword(value.email, value.password).then(res =>{ 
        this.User = true; 
        this.UIDuser = res.user.uid;
        if (this.AFauth.auth.currentUser.emailVerified == true) {
          resolve(res)
        } else {
          this.alertMessaje();
        }
      }).catch( err => reject(err)) 
    })    
  } 
//Mensaje de Validación de registro
  async alertMessaje(){
    const toast = await this.toastController.create({
      message: 'Revisa tu correo electronico en bandeja de entrada o spam para validar su registro',
      duration: 2000,
      position: 'middle', 
    });
    toast.present();
  }
//Register with email and password
  onRegister(value)
  {
    return new Promise ((resolve, reject) => {
      this.AFauth.auth.createUserWithEmailAndPassword(value.email, value.passwordRetry.passwordConfirmation).then( res =>{
        const uid = res.user.uid; // Nos da el uid (identificador de usuario)
        if (value.nit == null) {
          this.db.collection('users').doc(uid).set({
          nombre: value.nombre,
          apellido: value.apellido,
          });
        } else {
          this.db.collection('users').doc(uid).set({
            nit: value.nit,
            nombreEmpresa: value.nombreEmpresa,
            actividadEmpresa: value.actividadEmpresa,
            tipodocumento:value.tipodocumento,
            documento: value.documento,
            nombre: value.nombre,
            apellido: value.apellido,
            celular: value.celular,
            direccion: value.direccion,
          })
        }
        //Se guarda dentro de la autentificacion del firebase el nombre
        this.AFauth.auth.currentUser.updateProfile({
          displayName: value.nombre+' '+value.apellido,
        })
        //Envia un email de verificación
        this.AFauth.auth.currentUser.sendEmailVerification();
        this.alertMessaje();
        resolve(res)
      }).catch( err => reject(err));
    })
  }
//cerrar sesión 
  logout(){
    try {
      this.AFauth.auth.signOut().then(() => {
        this.googleplus.disconnect();
        this.facebook.logout();
        this.User = false; 
      })
    } catch (error) {
      console.log('Error on Logout user',error);
    }
      
  }
      
}

// import { Component } from '@angular/core'; 
// import { IonicPage, NavController, NavParams } from 'ionic-angular'; 
// import { User } from "../../models/user"; 
// import { AngularFireAuth } from "angularfire2/auth"; 
// import * as firebase from 'firebase'; @IonicPage() @Component({ selector: 'page-register', templateUrl: 'register.html', }) export class RegisterPage {
//    user = {} as User 
//    constructor(private afAuth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams)
//     { } 
//     async register(user: User) { 
//       try { 
//         const result = await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password) 
//         .then(res => { 
//           var auth = firebase.auth(); 
//           this.auth.sendEmailVerification(user.email) 
//           this.navCtrl.setRoot('LoginPage'); } } catch (e) { console.log(e); 
          
//           } } } 

//           let user = firebase.auth().currentUser; 
//           user.sendEmailVerification(); 
//           this.navCtrl.setRoot('LoginPage');

  //Register
  // onRegister(value)
  // {
  //   try {
  //     return new Promise ((resolve, reject) => {
  //           this.AFauth.auth.createUserWithEmailAndPassword(value.email, value.passwordRetry.password).then( res =>{
  //               // console.log(res.user.uid);
  //             const uid = res.user.uid;
  //               this.db.collection('users').doc(uid).set({
  //                 nit: value.nit,
  //                 nombreEmpresa: value.nombreEmpresa,
  //                 actividad: value.actividad,
  //                 tipodocumento:value.tipodocumento,
  //                 documento: value.documento,
  //                 nombre: value.nombre,
  //                 apellido: value.apellido,
  //                 pais:value.pais,
  //                 departamento:value.departamento,
  //                 municipio:value.municipio,
  //                 celular: value.celular,
  //                 telefono:value.telefono,
  //                 direccion:  value.direccion,
  //                 uid : uid
  //               })
  //             resolve(res)
  //           }).catch( err => reject(err))
  //         })
  //   } catch (error) {
  //     console.log('Error on Register user',error);
  //   }
  // }



  // loginUser(value) 
  // { return new Promise<any>((resolve, reject) => { 
  //   this.AFauth.auth.signInWithEmailAndPassword(value.correo, value.password).then( 
  //     res => resolve(res), err => reject(err)) }) 
  //   } 

    // registerUser(value) { return new Promise<any>((resolve, reject) => {
    //   this.AFauth.auth.createUserWithEmailAndPassword(value.email, value.password).then(
    //   res => resolve(res), err => reject(err)) }) 
    // } 
        

  // login(email:string, password:string){
  //   return new Promise((resolve, rejected) =>{
  //     this.AFauth.auth.signInWithEmailAndPassword(email, password).then(user => {
  //       resolve(user);
  //     }).catch(err => rejected(err));
  //   });
  // }

  // logout(){
  //   this.AFauth.auth.signOut().then(() => {
  //     this.router.navigate(['/login']);
  //   })
  // }

  // register(email : string, password : string){
  //   return new Promise ((resolve, reject) => {
  //     this.AFauth.auth.createUserWithEmailAndPassword(email, password).then( res =>{
  //         // console.log(res.user.uid);
  //       const uid = res.user.uid;
  //         this.db.collection('users').doc(uid).set({
  //           name : name,
  //           uid : uid
  //         })
  //       resolve(res)
  //     }).catch( err => reject(err))
  //   })
    
  // }


//  authentication.service.ts import { Injectable } from "@angular/core"; 
//  import { AngularFireAuth } from '@angular/fire/auth';
//   @Injectable({ providedIn: 'root' }) 
//  export class AuthenticateService { 
//    constructor( private afAuth: AngularFireAuth ) { } 
   
//    registerUser(value) { return new Promise<any>((resolve, reject) => {
//    this.afAuth.createUserWithEmailAndPassword(value.email, value.password) .then(
//       res => resolve(res), err => reject(err)) }) } 
      
//       loginUser(value) { return new Promise<any>((resolve, reject) => { 
//     this.afAuth.signInWithEmailAndPassword(value.email, value.password) .then( res => resolve(res), err => reject(err)) }) } 
    
//     logoutUser() { return new Promise((resolve, reject) => { if (this.afAuth.currentUser) { 
//       this.afAuth.signOut() .then(() => { console.log("LOG Out"); resolve(); }).catch((error) => { reject(); }); } }) } 
      
//       userDetails() { return this.afAuth.user } }