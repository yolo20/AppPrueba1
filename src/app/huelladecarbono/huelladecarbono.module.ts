import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule,  } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HuelladecarbonoPageRoutingModule } from './huelladecarbono-routing.module';

import { HuelladecarbonoPage } from './huelladecarbono.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HuelladecarbonoPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [HuelladecarbonoPage]
})
export class HuelladecarbonoPageModule {}
