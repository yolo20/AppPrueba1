import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HuelladecarbonoPage } from './huelladecarbono.page';

const routes: Routes = [
  {
    path: '',
    component: HuelladecarbonoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HuelladecarbonoPageRoutingModule {}
