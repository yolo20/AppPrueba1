import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HuelladecarbonoPage } from './huelladecarbono.page';

describe('HuelladecarbonoPage', () => {
  let component: HuelladecarbonoPage;
  let fixture: ComponentFixture<HuelladecarbonoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HuelladecarbonoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HuelladecarbonoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
