import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrodeproyectoPage } from './registrodeproyecto.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrodeproyectoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistrodeproyectoPageRoutingModule {}
