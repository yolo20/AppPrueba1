import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ToastController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FirestoreService } from '../services/data/firestore.service';

@Component({
  selector: 'app-registrodeproyecto',
  templateUrl: './registrodeproyecto.page.html',
  styleUrls: ['./registrodeproyecto.page.scss'],
})
export class RegistrodeproyectoPage implements OnInit {

  ResgistroInfGeneral : FormGroup;
  NumeroForm; // Variable que le entrega numero al form
  // objetos vacios para llenar con los datos del select pais departamento y municipio
  Departamentochoose = [];
  Municipiochoose = [];
  // objetos con los datos a mostrar de los select
  countries = ['Colombia'];//, 'Venezuela', 'Guyana', 'Surinam', 'Ecuador', 'Perú', 'Brasil', 'Bolivia', 'Paraguay', 'Chile', 'Uruguay', 'Argentina',];
  DepartamentoDePais = {
    Colombia: ['Amazonas', 'Antioquia', 'Arauca','Atlántico', 'Bolívar','Caldas', 'Caquetá','Casanare', 'Cauca','Cesar', 'Chocó','Córdoba', 'Cundinamarca','Guainía', 'Huila','La Guajira', 'Magdalena','Meta', 'Nariño',
    'Norte de Santander', 'Putumayo','Quindío', 'Risaralda','San Andrés y Providencia', 'Santander','Sucre', 'Tolima','Valle del Cauca', 'Vaupés','Vichada',],
  };
  MunicipioByDepartamento={
    Amazonas: ['El Encanto','La Chorrera','La Pedrera','La Victoria','Leticia','Mirití-Paraná','Puerto Alegría','Puerto Arica','Puerto Nariño','Puerto Santander','Tarapacá'], 
    Antioquia: ['Cáceres','Caucasia','El Bagre','Nechí','Tarazá','Zaragoza','Caracolí','Maceo','Puerto Berrío','Puerto Nare','Puerto Triunfo','Yondó','Amalfi','Anorí','Cisneros','Remedios','San Roque','Santo Domingo','Segovia','Vegachí','Yalí','Yolombó','Angostura','Belmira','Briceño','Campamento','Carolina del Príncipe',
    'Donmatías','Entrerríos','Gómez Plata','Guadalupe','Ituango','San Andrés de Cuerquia','San José de la Montaña','San Pedro de los Milagros','Santa Rosa de Osos','Toledo','Valdivia','Yarumal','Abriaquí','Antioquia','Anzá','Armenia','Buriticá',
    'Caicedo','Cañasgordas','Dabeiba','Ebéjico','Frontino','Giraldo','Heliconia','Liborina','Olaya','Peque','Sabanalarga','San Jerónimo','Sopetrán','Uramita','Abejorral','Alejandría','Argelia','El Carmen de Viboral','Cocorná','Concepción','El Peñol',
    'El Retiro','El Santuario','Granada','Guarne','Guatapé','La Ceja','La Unión','Marinilla','Nariño','Rionegro','San Carlos','San Francisco','San Luis','San Rafael','San Vicente','Sonsón','Amagá','Andes','Angelópolis','Betania','Betulia',
    'Caramanta','Ciudad Bolívar','Concordia','Fredonia','Hispania','Jardín','Jericó','La Pintada','Montebello','Pueblorrico','Salgar','Santa Bárbara','Támesis','Tarso','Titiribí','Urrao','Valparaíso','Venecia','Apartadó','Arboletes',
    'Carepa','Chigorodó','Murindó','Mutatá','Necoclí','San Juan de Urabá','San Pedro de Urabá','Turbo','Vigía del Fuerte','Barbosa','Bello','Caldas','Copacabana','Envigado','Girardota','Itagüí','La Estrella','Medellín','Sabaneta'],
    Arauca: ['Arauca','Arauquita','Cravo Norte','Fortul','Puerto Rondón','Saravena' ,'Tame' ],
    Atlántico: ['Barranquilla','Baranoa','Campo de la Cruz','Candelaria','Galapa','Juan de Acosta','Luruaco','Malambo','Manatí','Palmar de Varela','Piojó','Polonuevo','Ponedera','Puerto Colombia' ,'Repelón','Sabanagrande' ,
    'Sabanalarga','Santa Lucía','Santo Tomás','Soledad','Suan','Tubará','Usiacurí' ], 
    Bolívar: ['Achí','Altos del Rosario','Arenal','Arjona','Arroyohondo','Barranco de Loba','Calamar','Cantagallo','El Carmen de Bolívar','Cartagena de Indias','Cicuco','Clemencia','Córdoba','El Guamo' ,'El Peñón','Hatillo de Loba','Magangué','Mahates','Margarita' ,'María La Baja' ,
    'Montecristo','Morales','Norosí','Pinillos','Regidor','Río Viejo','San Cristóbal','San Estanislao','San Fernando','San Jacinto','San Jacinto del Cauca','San Juan Nepomuceno','San Martín de Loba','San Pablo','Santa Catalina','Santa Cruz de Mompox','Santa Rosa','Santa Rosa del Sur','Simití','Soplaviento','Talaigua Nuevo','Tiquisio','Turbaco','Turbaná','Villanueva','Zambrano' ], 
    Boyacá: ['Chíquiza','Chivatá','Cómbita' ,'Cucaita','Motavita','Oicatá','Samacá','Siachoque','Sora','Soracá','Sotaquirá','Toca','Tunja','Tuta' ,'Ventaquemada','Chiscas','El Cocuy','El Espino','Guacamayas','Güicán' ,
    'Panqueba' ,'Labranzagrande' ,'Pajarito' ,'Paya' ,'Pisba','Berbeo' ,'Campohermoso','Miraflores' ,'Páez' ,'San Eduardo' ,'Zetaquira' ,'Boyacá' ,'Ciénega' ,'Jenesano' ,'Nuevo Colón' ,'Ramiriquí' ,'Rondón' ,'Tibaná','Turmequé' ,'Úmbita' ,
    'Viracachá' ,'Chinavita','Garagoa','Macanal','Pachavita','San Luis de Gaceno' ,'Santa María','Boavita','Covarachía','La Uvita','San Mateo' ,'Sativanorte','Sativasur' ,'Soatá' ,'Susacón' ,'Tipacoque' ,'Briceño','Buenavista','Caldas','Chiquinquirá' ,'Coper','La Victoria','Maripí','Muzo' ,
    'Otanche','Pauna','Quípama','Saboyá','San Miguel de Sema','San Pablo de Borbur','Tununguá','Almeida','Chivor','Guateque','Guayatá','La Capilla','Somondoco','Sutatenza','Tenza','Arcabuco','Chitaraque','Gachantivá' ,'Moniquirá','Ráquira' ,
    'Sáchica','San José de Pare','Santa Sofía','Santana','Sutamarchán','Tinjacá','Togüí','Villa de Leyva' ,'Aquitania','Cuítiva','Firavitoba','Gámeza','Iza','Mongua','Monguí','Nobsa','Pesca','Sogamoso','Tibasosa','Tópaga','Tota', 
    'Belén','Busbanzá','Cerinza','Corrales','Duitama','Floresta','Paipa','Santa Rosa de Viterbo','Tutazá','Betéitiva','Chita','Jericó','Paz de Río','Socha','Socotá','Tasco','Cubará','Puerto Boyacá' ],
    Caldas: ['Filadelfia','La Merced','Marmato','Riosucio','Supía','Manzanares','Marquetalia','Marulanda','Pensilvania',
    'Anserma','Belalcázar','Risaralda','San José','Viterbo','Chinchiná','Manizales','Neira','Palestina','Villamaría','La Dorada','Norcasia','Samaná','Victoria','Aguadas','Aranzazu','Pácora','Salamina'],
    Caquetá: ['Albania','Belén de los Andaquíes','Cartagena del Chairá','Curillo','El Doncello','El Paujil','Florencia','La Montañita','Morelia','Puerto Milán','Puerto Rico','San José del Fragua','San Vicente del Caguán','Solano','Solita','Valparaíso' ],
    Casanare: ['Aguazul','Chámeza','Hato Corozal','La Salina','Maní','Monterrey','Nunchía','Orocué','Paz de Ariporo ','Pore','Recetor','Sabanalarga','Sácama','San Luis de Palenque','Támara','Tauramena','Trinidad','Villanueva','Yopal' ],
    Cauca: ['Buenos Aires','Caloto','Corinto','Guachené','Miranda','Padilla' ,'Puerto Tejada','Santander de Quilichao','Suárez','Villa Rica','Cajibío','El Tambo','La Sierra','Morales','Piendamó','Popayán', 
    'Rosas','Sotará','Timbío','Almaguer','Argelia','Balboa','Bolívar','Florencia','La Vega','Mercaderes','Patía','Piamonte','San Sebastián','Santa Rosa','Sucre','Guapi','López de Micay','Timbiquí','Caldono',
    'Inzá','Jambaló','Páez','Puracé - Coconuco','Silvia','Toribío','Totoró'],
    Cesar: ['Valledupar','Aguachica','Agustín Codazzi','Bosconia','Chimichagua','El Copey','San Alberto','Curumaní','El Paso','La Paz',
    'Pueblo Bello','La Jagua de Ibirico','Chiriguaná','Astrea' ,'San Martín','Pelaya','Pailitas' ,'Gamarra','Manaure Balcón del Cesar','Río de Oro','Tamalameque','Becerril','San Diego','La Gloria','González'],
    Chocó: [ 'Acandí','Alto Baudó','Atrato','Bagadó','Bahía Solano','Bajo Baudó','Bojayá','Cértegui','Condoto','El Cantón de San Pablo','El Carmen de Atrato','El Carmen del Darién','El Litoral de San Juan','Istmina','Juradó',
    'Lloró','Medio Atrato','Medio Baudó','Medio San Juan','Nóvita','Nuquí','Quibdó','Río Iró','Río Quito','Riosucio','San José del Palmar','Sipí','Tadó','Unguía','Unión Panamericana' ],
    Córdoba: ['Ayapel','Buenavista','Canalete','Cereté','Chimá' ,'Chinú','Ciénaga de Oro','Cotorra','La Apartada','Los Córdobas','Momil','Montelíbano','Montería','Moñitos','Planeta Rica','Pueblo Nuevo', 
    'Puerto Escondido','Puerto Libertador','Purísima','Sahagún','San Andrés de Sotavento','San Antero','San Bernardo del Viento','San Carlos','San José de Uré','San Pelayo','Santa Cruz de Lorica','Tierralta','Tuchín','Valencia'], 
    Cundinamarca: ['Chocontá','Machetá','Manta','Sesquilé','Suesca','Tibirita','Villapinzón','Agua de Dios','Girardot','Guataquí','Jerusalén','Nariño','Nilo','Ricaurte','Tocaima','Caparrapí','Guaduas','Puerto Salgar','Albán','La Peña','La Vega','Nimaima',
    'Nocaima','Quebradanegra','San Francisco','Sasaima','Supatá','Útica','Vergara','Villeta','Gachalá','Gachetá','Gama','Guasca','Guatavita','Junín','La Calera','Ubalá','Beltrán','Bituima','Chaguaní',
    'Guayabal de Síquima','Pulí','San Juan de Rioseco','Vianí','Medina','Paratebueno','Cáqueza','Chipaque','Choachí','Fómeque','Fosca','Guayabetal','Gutiérrez','Quetame','Ubaque','Une','El Peñón','La Palma','Pacho','Paime','San Cayetano',
    'Topaipí','Villagómez','Yacopí','Cajicá','Chía','Cogua','Cota','Gachancipá','Nemocón','Sopó','Tabio','Tenjo','Tocancipá','Zipaquirá','Bojacá','El Rosal','Facatativá','Funza','Madrid','Mosquera','Subachoque','Zipacón','Sibaté','Soacha','Arbeláez','Cabrera',
    'Fusagasugá','Granada','Pandi','Pasca','San Bernardo','Silvania','Tibacuy','Venecia','Anapoima','Anolaima','Apulo','Cachipay','El Colegio','La Mesa','Quipile','San Antonio del Tequendama','Tena','Viotá','Carmen de Carupa','Cucunubá','Fúquene','Guachetá','Lenguazaque','Simijaca','Susa','Sutatausa','Tausa','Ubaté' ], 
    Guainía: ['Barrancominas','Cacahual','Inírida','La Guadalupe','Morichal Nuevo','Pana Pana','Puerto Colombia','San Felipe'], 
    Guaviare: ['Calamar','El Retorno','Miraflores','San José del Guaviare'],
    Huila: ['Aipe','Algeciras','Baraya','Campoalegre','Colombia','Hobo','Íquira','Neiva','Palermo','Rivera','Santa María','Tello','Teruel','Villavieja',
    'Yaguará','Agrado','Altamira','Garzón','Gigante','Guadalupe','Pital','Suaza','Tarqui','La Argentina','La Plata','Nátaga','Paicol','Tesalia','Acevedo','Elías','Isnos','Oporapa','Palestina','Pitalito','Saladoblanco','San Agustín','Timaná' ],
    'La Guajira': ['Albania','Barrancas','Dibulla','Distracción','El Molino','Fonseca','Hatonuevo','La Jagua del Pilar','Maicao','Manaure','Riohacha','San Juan del Cesar','Uribia','Urumita','Villanueva' ], 
    Magdalena: ['Algarrobo','Aracataca','Ariguaní','Cerro de San Antonio','Chibolo','Ciénaga','Concordia','El Banco','El Piñón','El Retén','Fundación','Guamal','Nueva Granada','Pedraza','Pijiño del Carmen','Pivijay','Plato','Pueblo Viejo',
    'Remolino','Sabanas de San Ángel','Salamina','San Sebastián de Buenavista','Santa Ana','Santa Bárbara de Pinto','Santa Marta','San Zenón','Sitionuevo','Tenerife','Zapayán','Zona Bananera'], 
    Meta: ['Acacías','Barranca de Upía','Cabuyaro','Castilla La Nueva','Cubarral','Cumaral','El Calvario','El Castillo','El Dorado','Fuente de Oro','Granada','Guamal','La Macarena','La Uribe','Lejanías', 
    'Mapiripán','Mesetas','Puerto Concordia','Puerto Gaitán','Puerto Lleras','Puerto López','Puerto Rico','Restrepo','San Carlos de Guaroa','San Juan de Arama','San Juanito','San Martín','Villavicencio','Vista Hermosa'],
    Nariño: ['Barbacoas','El Charco','Francisco Pizarro','La Tola','Magüí Payán','Mosquera','Olaya Herrera','Roberto Payán','Santa Bárbara','Tumaco','Aldana','Contadero','Córdoba','Cuaspud',
    'Cumbal','Funes','Guachucal','Gualmatán','Iles','Ipiales','Potosí','Puerres','Pupiales','Albán','Arboleda','Belén','Colón','El Rosario','El Tablón de Gómez','La Cruz','La Unión',
    'Leiva','Policarpa','San Bernardo','San Lorenzo','San Pablo','San Pedro de Cartago','Taminango','Buesaco','Chachagüí','Consacá','El Peñol','El Tambo','La Florida','Nariño',
    'Pasto','Sandoná','Tangua','Yacuanquer','Ancuya','Cumbitara','Guaitarilla','Imués','La Llanada','Linares','Los Andes','Mallama','Ospina','Providencia','Ricaurte','Samaniego','Santacruz','Sapuyes','Túquerres'],
    'Norte de Santander': ['Arboledas','Cucutilla','Gramalote','Lourdes','Salazar de Las Palmas','Santiago','Villa Caro','Cúcuta','El Zulia','Los Patios','Puerto Santander','San Cayetano',
    'Villa del Rosario','Bucarasica','El Tarra','Sardinata','Tibú','Ábrego','Cáchira','Convención' ,'El Carmen','Hacarí','La Esperanza','La Playa de Belén','Ocaña','San Calixto','Teorama','Cácota','Chitagá','Mutiscua',
    'Pamplona','Pamplonita','Santo Domingo de Silos','Bochalema','Chinácota','Durania','Herrán','Labateca','Ragonvalia','Toledo'], 
    Putumayo: ['Colón','Mocoa','Orito','Puerto Asís','Puerto Caicedo','Puerto Guzmán','Puerto Leguízamo','San Francisco','San Miguel','Santiago','Sibundoy','Valle del Guamuez','Villagarzón'],
    Quindío: ['Armenia','Buenavista','Calarcá','Circasia','Córdoba','Filandia','Génova','La Tebaida','Montenegro','Pijao','Quimbaya','Salento'],
    Risaralda: ['Apía','Balboa','Belén de Umbría','Dosquebradas','Guática','La Celia','La Virginia','Marsella','Mistrató','Pereira','Pueblo Rico','Quinchía','Santa Rosa de Cabal','Santuario'],
    'San Andrés y Providencia': ['San Andrés','Providencia' , 'Santa Catalina'],
    Santander: ['Aguada','Albania','Aratoca','Barbosa','Barichara','Barrancabermeja','Betulia','Bolívar','Bucaramanga','Cabrera','California','Capitanejo','Carcasí','Cepitá','Cerrito',
    'Charalá','Charta','Chima','Chipatá','Cimitarra','Concepción','Confines','Contratación','Coromoro','Curití','El Carmen de Chucurí','El Guacamayo','El Peñón','El Playón','Encino',
    'Enciso','Florián','Floridablanca','Galán','Gámbita','Girón' ,'Guaca','Guadalupe','Guapotá','Guavatá','Güepsa','Hato','Jesús María','Jordán','La Belleza','La Paz','Landázuri','Lebrija', 
    'Los Santos', 'Macaravita','Málaga','Matanza' ,'Mogotes','Molagavita','Ocamonte','Oiba','Onzaga','Palmar','Palmas del Socorro','Páramo','Piedecuesta','Pinchote','Puente Nacional',
    'Puerto Parra','Puerto Wilches','Rionegro' ,'Sabana de Torres','San Andrés','San Benito','San Gil','San Joaquín','San José de Miranda','San Miguel','San Vicente de Chucurí','Santa Bárbara',
    'Santa Helena del Opón','Simacota','Socorro','Suaita','Sucre','Suratá','Tona','Valle de San José','Vélez','Vetas' ,'Villanueva','Zapatoca'],
    Sucre: ['Guaranda','Majagual' ,'Sucre' ,'Chalán' ,'Colosó' ,'Morroa' ,'Ovejas' ,'Sincelejo' ,'Coveñas' ,'Palmito','San Onofre' ,'Santiago de Tolú' ,'Toluviejo' ,'Buenavista' ,'Corozal' ,'El Roble' ,'Galeras' ,'Los Palmitos' ,'Sampués' ,'San Juan de Betulia ','San Pedro' ,'Sincé' ,
    'Caimito' ,'La Unión' ,'San Benito Abad' ,'San Marcos'],
    Tolima: [ 'Alvarado','Anzoátegui','Cajamarca','Coello','Espinal','Flandes','Ibagué','Piedras','Rovira','San Luis','Valle de San Juan','Casabianca','Herveo',
    'Lérida','Líbano','Murillo','Santa Isabel','Venadillo','Villahermosa','Ambalema','Armero','Falan','Fresno','Honda','San Sebastián de Mariquita','Palocabildo','Carmen de Apicalá','Cunday',
    'Icononzo','Melgar','Villarrica','Ataco','Chaparral','Coyaima','Natagaima','Ortega','Planadas','Rioblanco','Roncesvalles','San Antonio','Alpujarra','Dolores','Guamo','Prado','Purificación','Saldaña','Suárez'],
    'Valle del Cauca': ['Alcalá','Andalucía','Ansermanuevo','Argelia','Bolívar','Buenaventura','Buga','Bugalagrande','Caicedonia','Cali','Calima - El Darién','Candelaria','Cartago','Dagua','El Águila','El Cairo','El Cerrito',
    'El Dovio','Florida','Ginebra','Guacarí','Jamundí','La Cumbre','La Unión','La Victoria','Obando','Palmira','Pradera','Restrepo','Riofrío','Roldanillo','San Pedro','Sevilla','Toro','Trujillo','Tuluá','Ulloa',
    'Versalles','Vijes','Yotoco','Yumbo','Zarzal'],
    Vaupés: ['Carurú','Mitú' ,'Pacoa' ,'Papunaua' ,'Taraira' ,'Yavaraté' ],
    Vichada: ['Cumaribo','La Primavera' ,'Puerto Carreño' ,'Santa Rosalía'],
  }
   // objetos vacios para llenar con los datos del select 
   typepfprojects = ['Conservación','Creación']
   foresttype = [ 'Páramo','Bosque','Humedal']
   temperaturetype =[ '°C','°F' ] //Fahrenheit  Celsius​
   Currentusage = ['Recreación','Turismo','Senderismo','Abandonado','Siembra']
   zone = ['Rural','Urbano']
   treespecies = [];
   treesize = [];
   weather = [];
   floortype = [];


  constructor(
    private firestoreDB: FirestoreService, private navCtrl: NavController,
    private toastController: ToastController, private router: Router,
    private formBuilder: FormBuilder,  private _cdr: ChangeDetectorRef
    ) { }

    ngOnInit() {
      this.ResgistroInfGeneral = this.FromResgistroInfGeneral();
    }
//Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode){
    if (keyCode === 13) {
      if(this.ResgistroInfGeneral.valid){
        // Trigger the button element with a click
        document.getElementById("bttSubmitResgistroProyecto").click();
      }
    } 
  }
//Agreda datos a la DB CRUD
  CreateRecord(value) {
    console.log(this.ResgistroInfGeneral.value);
    this.firestoreDB.create(value,'RegistrarProyecto').then(resp => {
      this.ResgistroInfGeneral.reset(); // borramos los datos del form   
      this.router.navigate(['/folder/:id']);
    })
      .catch(error => {
        console.log(error);
      });
  }
//obtener datos del form
    // getDate(e) {
    //   let date = new Date(e.target.value).toISOString().substring(0, 10);
    //   this.UpdateEmpresa.get('dob').setValue(date, {
    //      onlyself: true
    //   }) 
    // }
// Donde se guardan los datos del form
  saveData(value){
    this.CreateRecord(value);
    this.firestoreDB.getNombreProyecto = value.nombreProyecto;
  }
  
// Condiciones de la entrada de datos form
  private FromResgistroInfGeneral(){
    return this.formBuilder.group({
      tipoproyecto:['', Validators.required],
      nombreProyecto:['', Validators.required],
      areaBosque:['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      tipoBosque:['', Validators.required],
      temperatura:['', [Validators.required, Validators.pattern('[0-9]*')]],
      tipoTemperatura:['', Validators.required],
      usoActual:['', Validators.required],
      pais:['', Validators.required],
      departamento:['', Validators.required],
      municipio:['', Validators.required],
      zona:['', Validators.required],
      msnm:['', [Validators.required, Validators.pattern('[0-9]*')]],
      latitud:['', [Validators.required, Validators.pattern('[0-9]*')]],
      longitud:['', [Validators.required, Validators.pattern('[0-9]*')]],
      nombrepredio:['', Validators.required],
      cedulacatastral:['', Validators.required],
      matricula:['', Validators.required],
      tipoSuelo:['', Validators.required],
      clima:['', Validators.required],
      diametroArbol:['', Validators.required],
      alturaArbol:['', Validators.required],
      edadArbol:['', Validators.required],
      tamanoArbol:['', Validators.required],
      especieArbol:['', Validators.required],

    });
  }
// Mesajes de error en las entradas de datos de los forms
  validation_messages = {
    'tipoproyecto':[
      
    ],
    'nombreProyecto':[
      
    ],
    'areaBosque':[
      
    ],
    'tipoBosque':[
      
    ],
    'temperatura':[
      
    ],
    'tipoTemperatura':[
      
    ],
    'usoActual':[
      
    ],
    'pais':[
      
    ],
    'departamento':[
      
    ],
    'municipio':[
      
    ],
    'zona':[
      
    ],
    'msnm':[
      
    ],
    'latitud':[
      
    ],
    'longitud':[
      
    ],
    'nombrepredio':[
      
    ],
    'cedulacatastral':[
      
    ],
    'matricula':[
      
    ],
    'tipoSuelo':[
      
    ],
    'clima':[
      
    ],
    'diametroArbol':[
      
    ],
    'alturaArbol':[
      
    ],
    'edadArbol':[
      
    ],
    'tamanoArbol':[
      
    ],
    'especieArbol':[
      
    ],
  }
// Activa el evento ionChange para introducir datos a los selects
  onPaisChange(){
    let pais = this.ResgistroInfGeneral.get('pais').value; //Pide el valor del Select pais
    this.Departamentochoose = this.DepartamentoDePais[pais]; // introduce en el Select las opciones de acuerdo a lo escogido
    document.getElementById('departamento').style.display="block";
    this._cdr.detectChanges();
  }
  DepartamentoChange(){
    let departamento = this.ResgistroInfGeneral.get('departamento').value;//Pide el valor del Select departamento
    this.Municipiochoose = this.MunicipioByDepartamento[departamento];// introduce en el Select las opciones de acuerdo a lo escogido
    document.getElementById('municipio').style.display="block";
    this._cdr.detectChanges();
  }
  

}
