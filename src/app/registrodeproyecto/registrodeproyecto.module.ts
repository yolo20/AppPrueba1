import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistrodeproyectoPageRoutingModule } from './registrodeproyecto-routing.module';

import { RegistrodeproyectoPage } from './registrodeproyecto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistrodeproyectoPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RegistrodeproyectoPage]
})
export class RegistrodeproyectoPageModule {}
