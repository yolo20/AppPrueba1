import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { FirestoreService } from '../services/data/firestore.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patrocinarproyecto',
  templateUrl: './patrocinarproyecto.page.html',
  styleUrls: ['./patrocinarproyecto.page.scss'],
})
export class PatrocinarproyectoPage implements OnInit {

  DatosPatrocinio: FormGroup;

  constructor(
    private firestoreDB: FirestoreService,private router: Router,
    private toastController: ToastController, private navCtrl: NavController,
    private formBuilder: FormBuilder,  private _cdr: ChangeDetectorRef
    ) { }

  ngOnInit() {
    this.DatosPatrocinio = this.FromAspLegales();
  }
//Agreda datos CRUD
  CreateRecord(value) {
    this.firestoreDB.create(value,'Patrocinar').then(resp => {
      this.DatosPatrocinio.reset(); // borramos los datos del form
    })
      .catch(error => {
        console.log(error);
      });
  }
//Evalua el uso del botton enter para hacer el submit
  // eventHandler(keyCode){
  //   if (keyCode === 13) {
  //     if(this.DatosPatrocinio.valid){
  //       // Trigger the button element with a click
  //       document.getElementById("bttSubmitPNatural").click();
  //     }
  //   } 
  // }
// Donde se guardan los datos del form
  saveData(value){
    this.CreateRecord(value);
    this.router.navigate(['/folder/:id']);
  }

// Condiciones de la entrada de datos form
  private FromAspLegales(){
    return this.formBuilder.group({
      datatimeInicio:['', Validators.required],
      datatimeFin: ['', Validators.required],
      rublo:['', Validators.required],
      cantidadDinero:['', [Validators.required, Validators.minLength(2)]],
      Frecuenciadelaporte:['', Validators.required],
      nombreProyecto: ['', Validators.required]
    });
  }
// Mesajes de error en las entradas de datos de los forms
  validation_messages = {
    'datatimeInicio':[
      
    ],
    'datatimeFin': [
      
    ],
    'rublo':[
      
    ],
    'cantidadDinero':[
      
    ],
    'Frecuenciadelaporte':[
      
    ],
    'nombreProyecto': [
      
    ]
  }


}
