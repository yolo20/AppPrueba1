import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PatrocinarproyectoPage } from './patrocinarproyecto.page';

describe('PatrocinarproyectoPage', () => {
  let component: PatrocinarproyectoPage;
  let fixture: ComponentFixture<PatrocinarproyectoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatrocinarproyectoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PatrocinarproyectoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
