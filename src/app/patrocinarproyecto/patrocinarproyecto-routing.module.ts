import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatrocinarproyectoPage } from './patrocinarproyecto.page';

const routes: Routes = [
  {
    path: '',
    component: PatrocinarproyectoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatrocinarproyectoPageRoutingModule {}
