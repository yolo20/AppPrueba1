import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatrocinarproyectoPageRoutingModule } from './patrocinarproyecto-routing.module';

import { PatrocinarproyectoPage } from './patrocinarproyecto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatrocinarproyectoPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [PatrocinarproyectoPage]
})
export class PatrocinarproyectoPageModule {}
