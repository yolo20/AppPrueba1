import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BasededatosPage } from './basededatos.page';

describe('BasededatosPage', () => {
  let component: BasededatosPage;
  let fixture: ComponentFixture<BasededatosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasededatosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BasededatosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
