import { Component, OnInit } from '@angular/core';
//Importamos el componente tost
import { ToastController, NavController } from '@ionic/angular';//Importamos el componente toast para mostrar un texto emergente
import { Validators, FormBuilder, FormGroup } from '@angular/forms'; // componentes forms
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';


@Component({
  selector: 'app-iniciarsesion',
  templateUrl: './iniciarsesion.page.html',
  styleUrls: ['./iniciarsesion.page.scss'],
})
export class IniciarsesionPage implements OnInit {

  hide = true;
  loginFrom: FormGroup;

  constructor(
    private authService: AuthService, private router: Router,
    private toastController: ToastController, private formBuilder: FormBuilder,
    private app: AppComponent,
    ) { }

  ngOnInit() {
    this.loginFrom = this.createMyForm();
  }
//Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode){
    if (keyCode === 13) {
      if(this.loginFrom.valid){
        // Trigger the button element with a click
        document.getElementById("bttSubmit").click();
      }
    } 
  }
// Login con Facebook
  LoginFacebook(){
    this.authService.loginWithFacebook().then(() =>{
      this.AccessWithLogin();
    }).catch(err => { alert('Algo salió mal contacta con soporte de usuario'); })
  }
// Login con Google
  LoginGoogle(){
    this.authService.loginWithGoogle().then(() =>{
      this.AccessWithLogin();
    }).catch(err => { alert('Algo salió mal contacta con soporte de usuario'); })
  }
//Login con Firebase (Correo y contraseña)
  loginUser(value)
  {
    this.authService.onLogin(value).then( () =>{
      this.AccessWithLogin();
       }).catch(err => { alert('los datos son incorrectos o no existe el usuario'); })
  }
//Modifica el menu principal
  AccessWithLogin(){
    this.router.navigate(['folder/:id']);
    this.app.selectedIndex = 0;
  }
// Donde se guardan los datos del form
  saveData(data){ 
  this.loginUser(data);
  }
// Condiciones de la entrada de datos form
  private createMyForm(){
    return this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*\\d)(?=.*[0-9])(?=.*[a-záéíóúüñ]).*[A-ZÁÉÍÓÚÜÑ].*')]]
    });
  }
// Mesajes de error en las entradas de datos de los forms
  validation_messages = {
    'email': [
      { type: 'required', message: 'Correo is required.' },
      { type: 'email', message: 'Please enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' },
      { type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number.' }
    ]
  }
}
